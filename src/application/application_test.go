package application

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestSubscribingToTopic(t *testing.T) {
	app := New(1)
	app.SubscribeToTopic("test")
	assert.Equal(t, 1, len(app.channels))
}

func TestTimeout(t *testing.T) {
	app := New(1)
	channel := app.SubscribeToTopic("test")
	time.Sleep(1001 * time.Millisecond)
	_, more := <-channel
	if more {
		assert.FailNow(t, "Expected channel to be closed after timeout")
	}
	assert.Equal(t, 0, len(app.channels))
}

func TestMessageReceiving(t *testing.T) {
	app := New(1)
	channel := app.SubscribeToTopic("test")
	go app.SendMessageAboutTopic("test", "text")
	message, more := <-channel
	if !more {
		assert.FailNow(t, "Expected channel not to be closed yet")
	}
	assert.Equal(t, "text", message.Text)
}

func TestMessageAboutDifferentTopic(t *testing.T) {
	app := New(1)
	channel := app.SubscribeToTopic("test")
	go app.SendMessageAboutTopic("2", "text")
	_, more := <-channel
	if more {
		assert.FailNow(t, "Expected channel to be closed without receiving message")
	}
}

func TestMessageIds(t *testing.T) {
	app := New(1)
	channel := app.SubscribeToTopic("test")
	go app.SendMessageAboutTopic("test", "text")
	message, more := <-channel
	if !more {
		assert.FailNow(t, "Expected channel not to be closed yet")
	}
	assert.Equal(t, int64(1), message.Id)

	go app.SendMessageAboutTopic("test", "text2")
	message, more = <-channel
	if !more {
		assert.FailNow(t, "Expected channel not to be closed yet")
	}
	assert.Equal(t, int64(2), message.Id)
}

func TestMessageAfterTimeout(t *testing.T) {
	app := New(1)
	channel := app.SubscribeToTopic("test")
	time.Sleep(1001 * time.Millisecond)
	go app.SendMessageAboutTopic("test", "text")
	_, more := <-channel
	if more {
		assert.FailNow(t, "Expected channel to be closed without receiving message")
	}
}

func TestChannelRemoving(t *testing.T) {
	app := New(1)
	channel := app.SubscribeToTopic("test")
	app.RemoveChannel(channel)
	_, more := <-channel
	if more {
		assert.FailNow(t, "Expected channel to be closed without receiving message")
	}
	assert.Equal(t, 0, len(app.channels))
}
