package application

import (
	"gitlab.com/shroote/sse/src/dto"
	"time"
)

type application struct {
	broadcastChannel   chan dto.Broadcast
	channels           map[chan dto.Message]string
	messageId          int64
	subscribeChannel   chan dto.Subscription
	timeout            int
	unsubscribeChannel chan chan dto.Message
}

func New(timeout int) *application {
	app := new(application)
	app.broadcastChannel = make(chan dto.Broadcast)
	app.channels = make(map[chan dto.Message]string)
	app.messageId = 0
	app.subscribeChannel = make(chan dto.Subscription)
	app.timeout = timeout
	app.unsubscribeChannel = make(chan chan dto.Message)

	go func() {
		for {
			select {
			case subscription := <-app.subscribeChannel:
				app.channels[subscription.Channel] = subscription.Topic
			case channel := <-app.unsubscribeChannel:
				// check to prevent closing of closed channel (if client disconnects early timeout still happens)
				_, ok := app.channels[channel]
				if ok {
					delete(app.channels, channel)
					close(channel)
				}
			case broadcastDto := <-app.broadcastChannel:
				app.messageId++
				messageDto := dto.Message{
					Id:   app.messageId,
					Text: broadcastDto.Message,
				}
				for c, t := range app.channels {
					if t == broadcastDto.Topic {
						// non-blocking send because it might be that client has disconnected already and channel is
						// waiting for removal
						select {
						case c <- messageDto:
						default:
						}
					}
				}
			}
		}
	}()

	return app
}

func (app *application) RemoveChannel(channel chan dto.Message) {
	app.unsubscribeChannel <- channel
}

func (app *application) SendMessageAboutTopic(topic, message string) {
	broadcastDto := dto.Broadcast{
		Message: message,
		Topic:   topic,
	}
	app.broadcastChannel <- broadcastDto
}

func (app *application) SubscribeToTopic(topic string) chan dto.Message {
	c := make(chan dto.Message)
	subscription := dto.Subscription{
		Channel: c,
		Topic:   topic,
	}

	go func() {
		ticker := time.NewTicker(time.Duration(app.timeout) * time.Second)
		defer ticker.Stop()

		<-ticker.C
		app.unsubscribeChannel <- subscription.Channel
	}()

	app.subscribeChannel <- subscription

	return c
}
