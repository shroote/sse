package dto

type Message struct {
	Id   int64
	Text string
}
