package dto

type Subscription struct {
	Channel chan Message
	Topic   string
}
