package dto

type Broadcast struct {
	Message string
	Topic   string
}
