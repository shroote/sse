package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/shroote/sse/src/application"
	"io/ioutil"
	"net/http"
)

const APP_PORT = 8001
const TIMEOUT = 30

var app = application.New(TIMEOUT)

func main() {
	fmt.Printf("Listening on port %d\n", APP_PORT)

	r := mux.NewRouter()
	r.HandleFunc("/infocenter/{topic}", sendMessage).Methods("POST")
	r.HandleFunc("/infocenter/{topic}", getMessages).Methods("GET")
	http.Handle("/", r)

	http.ListenAndServe(fmt.Sprintf(":%d", APP_PORT), nil)
}

// handler for message sending to a channel
func sendMessage(w http.ResponseWriter, r *http.Request) {
	// get required data from http request
	message, err := _getBody(r)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		w.WriteHeader(500)
		return
	}
	topic := _getUrlParameter(r, "topic")

	// pass data to application for actual handling
	app.SendMessageAboutTopic(topic, message)

	w.WriteHeader(204)
}

// handler for server-sent events
func getMessages(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("%s connected\n", r.RemoteAddr)

	// check that writer supports closed connection detecting & flushing
	closeNotifier, ok := w.(http.CloseNotifier)
	if !ok {
		fmt.Printf("error: no CloseNotify()\n")
		return
	}
	flusher, ok := w.(http.Flusher)
	if !ok {
		fmt.Printf("error: no Flush()\n")
		return
	}

	// set necessary headers for server-sent events
	_setSseHeaders(w)
	// get golang channel from which all messages will be received
	c := app.SubscribeToTopic(_getUrlParameter(r, "topic"))

	for {
		select {
		// client closed connection
		case <-closeNotifier.CloseNotify():
			fmt.Printf("%s disconnected\n", r.RemoteAddr)
			app.RemoveChannel(c)
			return
		case message, more := <-c:
			// timeout triggered in application
			if !more {
				_flushMessage(w, r, flusher, fmt.Sprintf("event: timeout\ndata: %ds\n\n", TIMEOUT))
				fmt.Printf("closing connection for %s\n", r.RemoteAddr)
				return
			}
			// actual message
			_flushMessage(w, r, flusher, fmt.Sprintf("id: %d\nevent: msg\ndata: %s\n\n", message.Id, message.Text))
		}
	}
}

// http helper methods
func _flushMessage(w http.ResponseWriter, r *http.Request, flusher http.Flusher, message string) {
	bytes := []byte(message)
	_, err := w.Write(bytes)
	if err != nil {
		fmt.Printf("error: write to %s failed\n", r.RemoteAddr)
		return
	}
	flusher.Flush()
}

func _getBody(r *http.Request) (string, error) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func _getUrlParameter(r *http.Request, title string) string {
	vars := mux.Vars(r)
	return vars[title]
}

func _setSseHeaders(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Content-Type", "text/event-stream")
}
